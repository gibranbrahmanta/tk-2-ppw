from django.urls import path
from . import views
from .views import detailBarang

appname = 'detailBarang'

urlpatterns = [
    path('<int:pk>', detailBarang, name='detailBarang')
]