from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import detailBarang
from sellItem.models import Category
from .models import *
import datetime, time

# Create your tests here.
class ProductDetailTest(TestCase):
    def test_product_url_is_exist(self):
        obj = Category.objects.create(
            name ='baju astrid',
            price = 0.000,
            category = "Dress",
            description = "Astrid udah halu",
            image = 'images/hnm.jpg'
        )
        response = Client().get('/product-detail/{}'.format(obj.pk))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'detailBarang/detail.html')

    def test_apakah_harga_sesuai(self):
        obj = Category.objects.create(
            name ='baju astrid',
            price = 0.000,
            category = "Dress",
            description = "Astrid udah halu",
            image = 'images/hnm.jpg'
        )
        field_name = 'price'
        field_value = getattr(obj, field_name)
        self.assertEqual(0.000, field_value)

    def test_apakah_deskripsi_sesuai(self):
        obj = Category.objects.create(
            name ='baju astrid',
            price = 0.000,
            category = "Dress",
            description = "Astrid udah halu",
            image = 'images/hnm.jpg'
        )
        field_name = 'description'
        obj = Category.objects.first()
        field_value = getattr(obj, field_name)#ini errorr
        self.assertEqual("Astrid udah halu", field_value)

    def test_apakah_bisa_rent_item(self):
        obj = Category.objects.create(
            name ='baju astrid',
            price = 0.000,
            category = "Dress",
            description = "Astrid udah halu",
            image = 'images/hnm.jpg'
        )
        rent = self.client.post('/product-detail/{}'.format(Category.objects.first().pk), data={
            'name': 'baju astrid',
            'price': 0.000,
        })
        name_obj = Productnya.objects.filter(name="baju astrid")
        self.assertTrue(name_obj.exists())

    def test_apakah_bisa_bikin_komentar(self):
        komentar = ProductDetail.objects.create(
            comment = "test komen"
        )
        hitung_semua_input = ProductDetail.objects.all().count()
        self.assertEqual(hitung_semua_input, 1)