atomicwrites==1.3.0
attrs==19.3.0
coverage==4.5.4
Django==2.0.7
django-bootstrap-form==3.4
django-filter==2.2.0
Faker==0.9.1
gunicorn==19.9.0
importlib-metadata==0.23
mixer==6.1.3
more-itertools==7.2.0
packaging==19.2
Pillow==6.2.0
pluggy==0.13.0
py==1.8.0
pyparsing==2.4.2
pytest==5.2.1
pytest-cov==2.8.1
pytest-django==3.6.0
python-dateutil==2.8.0
pytz==2019.2
six==1.12.0
text-unidecode==1.2
wcwidth==0.1.7
whitenoise==4.1.4
zipp==0.6.0
