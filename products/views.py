from django.shortcuts import render
from .forms import CategoryForm
from sellItem.models import Category
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def products(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if (form.is_valid()):
            if form.cleaned_data['Filter_by_category'] == 'All':
                items = Category.objects.all()
            else:
                items = Category.objects.filter(category = form.cleaned_data['Filter_by_category'])
        form = CategoryForm()
        context = {
            'formulir' : form,
            'items' : items
        }
        return render(request, 'products/products.html', context)
    else:
        form = CategoryForm()
        items = Category.objects.all()
        context = {
            'formulir' : form,
            'items' : items
        }
        return render(request, 'products/products.html', context)