from django import forms
from sellItem.models import Category

class CategoryForm(forms.Form):
    Filter_by_category = forms.ChoiceField(choices=Category.CATEGORY_CHOICES)