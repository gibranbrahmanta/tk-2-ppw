from django.test import TestCase, Client
from django.urls import resolve

from .models import Category
from .views import saveItem
from .form import addItem
# Create your tests here.

class sellItemUrlTests(TestCase):
	def test_sell_url(self):
		response = Client().get('/save-item/')
		self.assertEqual(response.status_code, 200)

	def test_redirect(self):
		response = Client().post('/save-item/')
		self.assertEqual(response.status_code, 200)

	def test_func(self):
		found = resolve('/save-item/')
		self.assertEqual(found.func, saveItem)

	def test_item_saving(self):
		test_item = Category(id=1, name = "baju",price=12.000, category = 'All', description="malih")
		test_item.save()

		self.assertIsInstance(test_item, Category)
		self.assertEqual(Category.objects.count(),1)

	def test_if_form_is_valid(self):
		form = addItem(data={ 'name': 'baju', 'prize' : 12.000, 'category' : 'All', 'description' :'malih' })
		self.assertTrue(form.is_valid)
