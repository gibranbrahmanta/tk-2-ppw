from django.urls import path
from .views import saveItem

app_name = 'sellItem'

urlpatterns= [
	path('', saveItem, name = 'saveItem')
]