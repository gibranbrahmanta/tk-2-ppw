# Generated by Django 2.0.7 on 2019-10-18 21:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sellItem', '0008_auto_20191018_2105'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='prize',
            new_name='price',
        ),
    ]
