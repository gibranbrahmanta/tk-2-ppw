from django.test import TestCase, Client
from django.test import Client
from django.test import SimpleTestCase
from django.urls import reverse, resolve
from django.http import HttpRequest
from .views import homepage

class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        response = Client().get('//')
        self.assertTemplateNotUsed(response, 'homepage.html')

    def test_text_exist(self):
        request = HttpRequest()
        response = homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Seller",html_response)
