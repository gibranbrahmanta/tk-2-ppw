from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout


# Create your views here.

def homepage(request):
    return render(request,'index.html')

def logout_view(request):
    logout(request)
    return render(request,'index.html')
