from django.urls import path
from .views import homepage, logout_view

appname = 'homepage'

urlpatterns = [
    path('', homepage, name='homepage'),
    path('',logout_view, name='logout'),
]