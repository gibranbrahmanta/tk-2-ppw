from django.test import TestCase, Client
from django.urls import resolve
from .views import startsell

# Create your tests here.
class startsellUrlTests(TestCase):
	def start_sell_url():
		response = Client().get('/start-sell/')
		self.assertEqual(response.status_code, 200)

	def test_func(self):
		found = resolve('/start-sell/')
		self.assertEqual(found.func, startsell)

	def test_redirect(self):
		response = Client().post('/start-sell/')
		self.assertEqual(response.status_code, 200)


	