from django.test import TestCase, Client
from django.test import Client
from django.test import SimpleTestCase
from django.urls import reverse, resolve
from django.http import HttpRequest
from .forms import CreateCartForm
from .models import Cart
from .views import itemcart

class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/item-list/')
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        response = Client().get('/item-list/')
        self.assertTemplateNotUsed(response, 'item.html')

    def test_text_exist(self):
        request = HttpRequest()
        response = itemcart(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Items you Rented",html_response)

    def test_correct_views_used(self):
        found = resolve('/item-list/')
        self.assertEqual(found.func, itemcart)

    def test_model(self):
        Cart.objects.create(cart_name='lorem ipsom')
        count = Cart.objects.all().count()
        self.assertEqual(count, 1)

    def test_form_valid(self):
        data = {'Cart_name':'lorem ipsom'}
        form = CreateCartForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['Cart_name'],"lorem ipsom")

