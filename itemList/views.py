from django.shortcuts import render
from .models import Cart
from .forms import CreateCartForm
from detailBarang.models import Productnya

# Create your views here.
# def itemcart(request):
#     return render(request, 'schedule.html')

def itemcart(request):
    if request.method == "POST":
        form = CreateCartForm(request.POST)
        if (form.is_valid()):
            products = Productnya.objects.all()
            cart = Cart()
            cart.cart_name = form.cleaned_data['Cart_name']
            for objects in products:
                cart.items.append(objects)
            cart.save()
        form = CreateCartForm()
        products = Productnya.objects.all()
        context = {
            'formulir' : form,
            'products' : products
        }
        return render(request, "itemlist.html", context)
    else:
        form = CreateCartForm()
        products = Productnya.objects.all()
        context = {
            'formulir' : form,
            'products' : products
        }
        return render(request, "itemlist.html", context)

def remove(request, id, *args, **kwargs):
    Productnya.objects.filter(id=id).delete()

    if request.method == "POST":
        form = CreateCartForm(request.POST)
        if (form.is_valid()):
            products = Productnya.objects.all()
            cart = Cart()
            cart.cart_name = form.cleaned_data['Cart_name']
            for objects in products:
                cart.items.append(objects)
            cart.save()
        form = CreateCartForm()
        products = Productnya.objects.all()
        context = {
            'formulir' : form,
            'products' : products
        }
        return render(request, "itemlist.html", context)
    else:
        form = CreateCartForm()
        products = Productnya.objects.all()
        context = {
            'formulir' : form,
            'products' : products
        }
        return render(request, "itemlist.html", context)
