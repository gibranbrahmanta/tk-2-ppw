from django.urls import path
from django.conf.urls import url
from .views import itemcart, remove

appname = 'itemList'

urlpatterns = [
    path('', itemcart, name="itemcart"),
    path('<int:id>', remove, name="remove")
]