from django.apps import AppConfig


class ItemlistConfig(AppConfig):
    name = 'itemList'
