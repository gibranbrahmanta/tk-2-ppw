from django.urls import path
from . import views
from .views import signup_view

appname = 'login'

urlpatterns = [
    path('', signup_view, name='signup'),
]