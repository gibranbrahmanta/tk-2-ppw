from django.shortcuts import render, redirect
from .forms import SignupForm, LoginForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages

# Create your views here.
def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
        else:
            for msg in form.error_messages:
                messages.error(request, 'Invalid entry')
    else:
        form = SignupForm()
    context = {
        'form' : form
    }
    return render(request, 'otentikasi/signup.html', context)