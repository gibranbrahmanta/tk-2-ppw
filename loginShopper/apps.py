from django.apps import AppConfig


class LoginshopperConfig(AppConfig):
    name = 'loginShopper'
