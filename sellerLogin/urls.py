from django.urls import path
from . import views

appname = 'sellerLogin'

urlpatterns = [
    path('', views.index, name='sellerLogin'),
]