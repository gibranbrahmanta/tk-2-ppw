from django.test import TestCase, Client
from django.http import HttpRequest
from .forms import MessageForm
from .views import approval_list

# Create your tests here.
class approvalList(TestCase):
    def test_landing_page(self):
        response = Client().get("/approval-list/")
        self.assertEqual(response.status_code,200)
    
    def test_landing_page_template(self):
        response = Client().get("/approval-list/")
        self.assertTemplateUsed(response,'approvalList.html')

    def test_welcome_text(self):
        request = HttpRequest()
        response = approval_list(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Back to Seller Page",html_response)
    
    def test_form_valid(self):
        data = {"message_for_user":"abc"}
        form = MessageForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['message_for_user'],"abc")
