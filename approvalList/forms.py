from django import forms
from itemList.models import Cart

class MessageForm(forms.Form):
    message_for_user = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter a message',
        'type' : 'text',
        'required': True,
    }))

class UserForm(forms.Form):
    NAMES = []
    carts = Cart.objects.all()
    for items in carts:
        NAMES.append(items.cart_name)

    Find_by_user = forms.ChoiceField(choices=NAMES)

class CartNameForm(forms.Form):
    Cart_name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Your cart name',
        'type' : 'text',
        'required' : True
    }))