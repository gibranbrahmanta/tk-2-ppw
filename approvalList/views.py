from django.shortcuts import render,redirect 
from .forms import MessageForm, UserForm, CartNameForm
from itemList.models import Cart

def approval_list(request):
    if request.method == 'POST':
        form = CartNameForm(request.POST)
        if form.is_valid():
            cart = Cart.objects.get(cart_name = form.cleaned_data['Cart_name'])
        context ={
            'form' : form,
            'cart' : cart
        }
        print(cart.items)
        return render(request, 'approvalList.html', context)
    else:
        form = CartNameForm()
        context ={
            'form' : form,
        }
        return render(request, 'approvalList.html', context)